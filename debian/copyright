Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ledgerhelpers
Upstream-Contact: Manuel Amador (Rudd-O) <rudd-o@rudd-o.com>
Source: https://github.com/Rudd-O/ledgerhelpers

Files: *
Copyright: 2015-2022 Manuel Amador (Rudd-O) <rudd-o@rudd-o.com>
License: GPL-2.0+

Files: src/ledgerhelpers/dateentry.py
Copyright: 2006-2012 Async Open Source
           2016-2019 Manuel Amador (Rudd-O) <rudd-o@rudd-o.com>
License: LGPL-2.1+
Comment: This file was initially copied from the kiwi toolkit (which is
 provided under the LGPL 2.1 or later license terms), and edited later.
 .
 Judging by size of diff to the first version in this project, the copied file
 was https://github.com/stoq/kiwi/blob/d61a55b85540a3a8a07e9e871d36a1a4d9c6a1fa/kiwi/ui/dateentry.py

Files: debian/*
Copyright: 2022 Marcin Owsiany <porridge@debian.org>
License: GPL-2.0+

Files: man/*
Copyright: 2022 Marcin Owsiany <porridge@debian.org>
License: GPL-2.0+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
