Source: ledgerhelpers
Section: utils
Priority: optional
Maintainer: Marcin Owsiany <porridge@debian.org>
Build-Depends: debhelper-compat (= 13), dh-python, python3-all, python3-setuptools, python3-ledger <!nocheck>, python3-markdown, pybuild-plugin-pyproject (>= 5.20221205)
Standards-Version: 4.6.1
Homepage: https://github.com/Rudd-O/ledgerhelpers
Vcs-Browser: https://salsa.debian.org/python-team/packages/ledgerhelpers
Vcs-Git: https://salsa.debian.org/python-team/packages/ledgerhelpers.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: ledgerhelpers
Architecture: all
Depends: python3-ledger, ${python3:Depends}, ${misc:Depends}
Enhances: ledger
Recommends: ledger
Suggests: meld
Description: collection of helper programs and a helper library for ledger
 This is a collection of small single-purpose programs to aid your accounting
 with ledger. Think of it as the batteries that were never included with
 ledger. What can you do with these programs:
 .
  - Enter transactions easily with addtrans.
  - Record multi-currency ATM withdrawals with withdraw-cli.
  - Record FIFO stock or commodity sales with sellstock-cli.
  - Interactively clear transactions with cleartrans-cli.
  - Keep your ledger chronologically sorted with sorttrans-cli.
